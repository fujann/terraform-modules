# how should the app be named
variable "app" {}

# How many containers to run
variable "instances" {
  default = "1"
}
variable "ecs_autoscale_min_instances" {
  default = "1"
}
variable "ecs_autoscale_max_instances" {
  default = "2"
}

# The name of the container to run
variable "container_name" {
  default = "app"
}

variable "memory" {
  default = "1024"
}
variable "cpu" {
  default = "512"
}
variable "container_port" {}

variable "tags" {
  type = "map"
}
variable "vpcid" {}

variable "private_subnets" {
  type = "list"
}
variable "public_subnets" {
  type = "list"
}

variable "wildcard_certificate_arn" {
  default = "arn:aws:acm:eu-central-1:865081723449:certificate/fe05e88f-7ce5-4fa3-84bd-5cb8f6aae4a9"
}

# The default docker image to deploy with the infrastructure.
variable "service_image" {}

