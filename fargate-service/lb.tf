# note that this creates the alb, target group, and access logs
# the listeners are defined in lb-http.tf and lb-https.tf
# delete either of these if your app doesn't need them
# but you need at least one


# The path to the health check for the load balancer to know if the container(s) are ready
variable "health_check" {}

resource "aws_alb" "main" {
  name = "${var.app}-${terraform.workspace}"

  # launch lbs in public or private subnets based on "internal" variable
  internal        = "false"
  subnets         = ["${var.public_subnets}"]
  security_groups = ["${aws_security_group.nsg_lb.id}"]
  tags            = "${var.tags}"

  # enable access logs in order to get support from aws
  access_logs {
    enabled = true
    bucket  = "${aws_s3_bucket.lb_access_logs.bucket}"
  }
}

resource "aws_alb_target_group" "main" {
  name                 = "${var.app}-${terraform.workspace}"
  port                 = "80"
  protocol             = "HTTP"
  vpc_id               = "${var.vpcid}"
  target_type          = "ip"
  deregistration_delay = "30"

  health_check {
    path                = "${var.health_check}"
    matcher             = "200"
    interval            = "30"
    timeout             = "10"
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  tags = "${var.tags}"
}

data "aws_elb_service_account" "main" {}

# bucket for storing ALB access logs
resource "aws_s3_bucket" "lb_access_logs" {
  bucket        = "seatris-${var.app}-${terraform.workspace}-lb-access-logs"
  acl           = "private"
  tags          = "${var.tags}"
  force_destroy = true

  lifecycle_rule {
    id                                     = "cleanup"
    enabled                                = true
    abort_incomplete_multipart_upload_days = 1
    prefix                                 = "/"

    expiration {
      days = 3
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

# give load balancing service access to the bucket
resource "aws_s3_bucket_policy" "lb_access_logs" {
  bucket = "${aws_s3_bucket.lb_access_logs.id}"

  policy = <<POLICY
{
  "Id": "Policy",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.lb_access_logs.arn}",
        "${aws_s3_bucket.lb_access_logs.arn}/*"
      ],
      "Principal": {
        "AWS": [ "${data.aws_elb_service_account.main.arn}" ]
      }
    }
  ]
}
POLICY
}

# adds an https listener to the load balancer

resource "aws_alb_listener" "https" {
  load_balancer_arn = "${aws_alb.main.id}"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = "${var.wildcard_certificate_arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.main.id}"
    type             = "forward"
  }
}

resource "aws_security_group_rule" "ingress_lb_https" {
  type              = "ingress"
  description       = "HTTPS"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.nsg_lb.id}"
}

# The load balancer DNS name
output "lb_dns" {
  value = "${aws_alb.main.dns_name}"
}
