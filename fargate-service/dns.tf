
data "aws_route53_zone" "selected" {
  name         = "seatris.io."
  private_zone = false
}

resource "aws_route53_record" "www-dev" {
  zone_id = "${data.aws_route53_zone.selected.zone_id}"
  name    = "${var.app}${terraform.workspace == "prod" ? "" : format("-%s", terraform.workspace)}"
  type    = "CNAME"
  ttl     = "5"
  records = ["${aws_alb.main.dns_name}"]
}
